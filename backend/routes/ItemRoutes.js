import { Router } from "express";
import ItemRepository from "../repositories/ItemRepository";

const routes = Router();

routes
  .route("/")
  .get((req, res) => {
    ItemRepository.findAll()
      .then(items => {
        res.json(items);
      })
      .catch(error => console.log(error));
  })
  .post((req, res) => {
    const { aion_id, name, icon } = req.body;
    ItemRepository.create(aion_id, name, icon)
      .then(item => {
        res.json(item);
      })
      .catch(error => console.log(error));
  });

routes
  .route("/:id")
  .get((req, res) => {
    ItemRepository.findById(id)
      .then(item => {
        res.json(item);
      })
      .catch(error => console.log(error));
  })

  .put((req, res) => {
    const { id } = req.params;
    const item = {
      aion_id: req.body.aion_id,
      name: req.body.name,
      icon: req.body.icon
    };
    ItemRepository.updateById(id, item)
      .then(res.status(200).json([]))
      .catch(error => console.log(error));
  })

  .delete((req, res) => {
    const { id } = req.params;
    ItemRepository.deleteById(id)
      .then(ok => {
        console.log(ok);
        console.log(`Deleted record with id: ${id}`);
        res.status(200).json([]);
      })
      .catch(error => {
        console.log(error);
      });
  });

export default routes;
