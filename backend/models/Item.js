import * as mongoose from 'mongoose'
const { Schema } = mongoose
const itemSchema = new Schema({ aion_id: Number, name: String, icon: String })
const Item = mongoose.model('Item', itemSchema)
export default Item
