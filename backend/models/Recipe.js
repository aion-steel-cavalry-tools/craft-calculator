import * as mongoose from 'mongoose'
import { Item } from './Item'
const { Schema } = mongoose

const recipeSchema = new Schema({
  aion_id: Number,
  name: String,
  materials: [Item],
  product: Item,
})

const Recipe = mongoose.model('Recipe', recipeSchema)

export default Recipe
