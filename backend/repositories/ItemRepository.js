import Item from "../models/Item";

class ItemRepository {
  constructor(model) {
    this.model = model;
  }

  // create a new todo
  create(aion_id, name, icon) {
    const newItem = { aion_id, name, icon };
    const item = new this.model(newItem);

    return item.save();
  }

  // return all todos

  findAll() {
    return this.model.find();
  }

  // find todo by the id
  findById(id) {
    return this.model.findById(id);
  }

  // delete todo
  deleteById(id) {
    return this.model.findByIdAndDelete(id);
  }

  // update todo
  updateById(id, object) {
    const query = { _id: id };
    return this.model.findOneAndUpdate(query, {
      $set: { aion_id: object.aion_id, name: object.name, icon: object.icon }
    });
  }
}

export default new ItemRepository(Item);
